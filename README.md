# docker-maven-git

Maven 3 and OpenJDK (Amazon Corretto) with Git for CI/CD pipelines in Docker

Based on https://hub.docker.com/_/maven . Just adding Git.

```
docker pull wrzlbrmft/maven-git:<version>
```

See also:

  * https://maven.apache.org/
  * https://openjdk.org/
  * https://aws.amazon.com/corretto
  * https://git-scm.com/
  * https://hub.docker.com/r/wrzlbrmft/maven-git/tags

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
