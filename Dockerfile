ARG TAG=3-amazoncorretto-21
FROM maven:${TAG}

RUN yum -y install \
        git \
    && yum clean all
